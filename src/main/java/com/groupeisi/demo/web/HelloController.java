package com.groupeisi.demo.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController  {
    @GetMapping
    public ResponseEntity<String> message() {
        return new ResponseEntity<>("Bonne et heureuse annee 2024", HttpStatus.OK);
    }
}
